#!/bin/sh

for i in {10..39}; do 
	aws iam create-user --user-name ep$i;
	aws iam create-login-profile --user-name ep$i --password P@ss$i; 
done