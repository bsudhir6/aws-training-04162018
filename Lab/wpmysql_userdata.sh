#!/bin/bash

apt-get update -y

debconf-set-selections <<< 'mysql-server mysql-server/root_password password TopSecret#123'

debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password TopSecret#123'

apt-get -y install mysql-server -y

echo "Updating mysql configs in /etc/mysql/my.cnf."

sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

echo "Updated mysql bind address in /etc/mysql/my.cnf to 0.0.0.0 to allow external connections."

echo "Assigning mysql user wordpressuser access on %."

service mysql restart

mysql -u root -pTopSecret#123 --execute "create database wordpress character set utf8 collate utf8_bin;"

mysql -u root -pTopSecret#123 --execute "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpressuser'@'%' IDENTIFIED BY 'Welcome#123' with GRANT OPTION; FLUSH PRIVILEGES;" 

echo "Assigned mysql user wordpressuser access on all hosts."

service mysql restart

echo "mysql is running ......"

apt-get install  apache2 -y

apt-get install libapache2-mod-auth-mysql -y

apt-get install php libapache2-mod-php php-mcrypt php-mysql -y

service apache2 restart

cd /home/ubuntu

wget http://wordpress.org/latest.tar.gz

tar -xzvf latest.tar.gz

cp /home/ubuntu/wordpress/wp-config-sample.php /home/ubuntu/wordpress/wp-config.php

sed -i "s/.*define('DB_NAME', 'database_name_here');.*/define('DB_NAME', 'wordpress');/" /home/ubuntu/wordpress/wp-config.php

sed -i "s/.*define('DB_USER', 'username_here');.*/define('DB_USER', 'wordpressuser');/" /home/ubuntu/wordpress/wp-config.php

sed -i "s/.*define('DB_PASSWORD', 'password_here');.*/define('DB_PASSWORD', 'Welcome#123');/" /home/ubuntu/wordpress/wp-config.php

echo "define('FORCE_SSL_ADMIN', true);" >> /home/ubuntu/wordpress/wp-config.php

echo "if (strpos(\$_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)" >> /home/ubuntu/wordpress/wp-config.php

echo "\$_SERVER['HTTPS']='on';" >> /home/ubuntu/wordpress/wp-config.php

cp -r /home/ubuntu/wordpress/* /var/www/html

rm /var/www/html/index.html

echo "Wordpress Installation Complete at Terminal. Continue the remaining part on the browser"

chown -R www-data:www-data /var/www/html

service apache2 restart


echo "Wordpress is now running ...."



